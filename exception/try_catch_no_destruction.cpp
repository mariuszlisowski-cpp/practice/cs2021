#include <iostream>
#include <stdexcept>

struct TalkingObjectA {
    TalkingObjectA()  { std::cout << "A c'tor" << '\n'; }
    ~TalkingObjectA() { std::cout << "A d'tor" << '\n'; }
};

struct TalkingObjectB {
    TalkingObjectB()  { std::cout << "B d'tor" << '\n'; }
    ~TalkingObjectB() { std::cout << "B tor" << '\n'; }
};

void foo() { throw std::runtime_error("Error"); }               // runtime throw

void bar() {
    try {
        TalkingObjectB inside;                                  // NOT destructed
        foo();
    } catch(std::logic_error const&) {                          // other exeception type
        std::cout << "std::logic_error" << '\n';                // logic expected
    }
}

int main() {
    TalkingObjectA outside;
}
