#include <iostream>

int main() {
    int number = 9;

    auto result = [&number](auto num) {                     // generic lambda
        return num * num;                                   // accepting any argument type
    };

    std::cout << result(9) << std::endl;
    std::cout << result(2.15) << std::endl;

    return 0;
}
