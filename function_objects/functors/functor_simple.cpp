#include <iostream>

class Foo {
public:
    void operator()() {
        std::cout << "> I'm a functor" << std::endl;
    }

};

int main() {
    Foo functor;
    functor();                                              // call named object
    functor();                                              // many times

    Foo{}();                                                // call temporary object (once)

    return 0;
}
