#include <algorithm>
#include <iostream>
#include <vector>

struct Foo {
    void operator()(int el) {                                   // object is a functor now
        std::cout << index << ": " << el << " | ";              // as it may be called by using () operator
        ++index;
    }

    int index{};                                                // functor can have a STATE
};

void print(int el) {
    std::cout << el << ' ';
}

void newline() {
    std::cout << std::endl;
}

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5 };

    /* functor */
    std::for_each(begin(v), end(v), Foo{});                     // create temporary functor many times
                                                                // which changes its STATE
                                                                // but only for this algorithm
    newline();

    Foo foo;                                                    // object (functor)
    std::for_each(begin(v), end(v), foo);                       // call existing functor many times
    newline();
    
    std::cout << "index: " << foo.index << std::endl;           // STATE not changed

    std::for_each(begin(v), end(v), foo);
    newline();

    /* pointer */
    std::for_each(begin(v), end(v), print);                     // call function instead functor
    newline();

    return 0;
}
