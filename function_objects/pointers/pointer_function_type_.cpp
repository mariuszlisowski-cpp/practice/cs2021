#include <iostream>
#include <vector>

void function() {
    std::cout << "> function called" << std::endl;
}

int triple(int value) {
    return 3 * value;
}

int main() {
    auto f_auto = function;                             // automatic type decuction
    void (*f_manu)() = function;                        // manual type
    f_auto();
    f_manu();

    auto t_auto = triple;                               // automatic type decuction
    int (*t_manu)(int) = triple;                        // manual type
    std::cout << t_manu(3) << std::endl;

    int (*example)(double, std::vector<int>) = nullptr;
    if (!example) {
        std::cout << "> nullptr" << std::endl;
    } else {
        example(7, {1, 2, 3});                          // safe to call
    }

    return 0;
}
