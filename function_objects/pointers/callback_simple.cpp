// g++ -fconcepts .\callback.cpp
#include <iostream>

void function(auto ptr_to_function) {
    // do some work

    ptr_to_function();                              // callback
}

void finish() {
    std::cout << "> callback used" << std::endl;
}

int main() {
    function(finish);                               // pointer to function passed

    return 0;
}
