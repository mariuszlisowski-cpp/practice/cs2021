// Mateusz Adamski CS lesson

#include <algorithm>
#include <functional>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

class Foo {
public:
    int val_{20};

    int doSth(const std::string& str, int val) {
        return std::stoi(str) + val;
    }

    void callbackHandler(int val) {
        std::cout << "Got value: " << val << '\n';
        // print Bitmap
    }
};

int blabla(const std::string& str, int val) {
    std::cout << "str: " << str << " | val: " << val << '\n';
    return 5;
}

int (*blablaa)(const std::string&, int) = blabla;

void modifyFoo(int Foo::*ptr, Foo& foo, int new_val) {
    foo.*ptr = new_val;
}

void modifyFooPtr(int Foo::*ptr, Foo* foo, int new_val) {
    foo->*ptr = new_val;
}

std::function<int(Foo&, const std::string&, int)> ptr = &Foo::doSth;

auto myInvoke(std::function<int(Foo&, const std::string&, int)> fun, Foo& foo, const std::string& str, int val) {
    return fun(foo, str, val);
}

int main() {
    // Pointer to class member
    {
        int Foo::*ptr;

        Foo foo;
        std::cout << foo.val_ << '\n';
        foo.val_ = 50;
        std::cout << foo.val_ << '\n';

        ptr = &Foo::val_;
        foo.*ptr = 100;
        std::cout << foo.val_ << '\n';

        modifyFoo(ptr, foo, 200);
        std::cout << foo.val_ << '\n';
        modifyFooPtr(ptr, &foo, 300);
        std::cout << foo.val_ << '\n';
    }
    // Pointer to class method
    {
        std::cout << std::string(80, '_') << '\n';
        Foo foo;

        int (Foo::*ptr_to_function)(const std::string&, int);
        ptr_to_function = &Foo::doSth;
        std::cout << (foo.*ptr_to_function)("100", 28) << '\n';

        std::cout << myInvoke(ptr_to_function, foo, "1000", 24) << '\n';
    }
    // Invoke
    {
        std::cout << std::string(80, '_') << '\n';
        Foo foo;
        int Foo::*ptr;
        ptr = &Foo::val_;
        std::cout << foo.val_ << '\n';

        std::invoke(ptr, &foo) = 500;
        std::cout << std::invoke(ptr, foo) << '\n';

        int (Foo::*ptr_to_function)(const std::string&, int);
        ptr_to_function = &Foo::doSth;
        std::cout << std::invoke(ptr_to_function, foo, "2000", 48) << '\n';
        std::cout << std::invoke(ptr_to_function, &foo, "4000", 96) << '\n';

        std::invoke(modifyFoo, ptr, foo, 1000);
        std::cout << foo.val_ << '\n';
    }
    // Bind
    {
        std::vector<std::function<void()>> fun_;

        std::string str = "Ala";
        auto lambda = [&str](int val) {
            blabla(str, val);
        };
        for (int i = 0; i < 10; ++i) {
            lambda(i);
        }

        auto lambda2 = [&str]() {
            blabla(str, 40);
        };
        lambda2();

        fun_.push_back(lambda2);

        auto funn = std::bind(blabla, std::placeholders::_1, 100);
        funn("kot");
        auto funn2 = std::bind(blabla, "Kot", 200);
        funn2();

        fun_.push_back(funn2);
        std::cout << std::string(80, '_') << '\n';
        for (auto&& function : fun_) {
            function();
        }

        std::cout << std::string(80, '_') << '\n';
        Foo foo;
        auto lambdaWithCollback = [&str, &foo](){
            foo.callbackHandler(blabla(str, 40));
        };
        lambdaWithCollback();

        // Write a function which take 2 arguments (float, std::string) and callback
        // This function should call some method and pass 2 arguments and call callback with result
        // void fun (float f, std::string str, auto callback) {
        //     makeConnection();
        //     auto bitmap = downloadImage(str);
        //     callback(convertToSize(bitmap, f));
        // }
    }

    return 0;
}
