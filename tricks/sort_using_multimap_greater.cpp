#include <iostream>
#include <set>
#include <vector>

int main() {
    std::vector<int> v{ 3, 4, 6, 2, 3, 1, 0, 9 };

    std::multiset<int, std::greater<int>> s(v.begin(), v.end());            // sorted with duplicates
                                                                            // plus comparator
    for (const auto el : s) {
        std::cout << el;
    }

    return 0;
}
