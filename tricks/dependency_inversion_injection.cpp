/* by Mateusz Adamski for Coders School */

#include <chrono>
#include <filesystem>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <random>
#include <thread>
#include <vector>
#include <unordered_map>

std::vector<std::vector<uint16_t>> getBitmap(uint16_t width, uint16_t height) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dist(10, 99);

    std::vector<std::vector<uint16_t>> bitmap(height);
    std::generate(begin(bitmap), end(bitmap), [&]() {
        std::vector<uint16_t> new_row(width);
        std::generate(begin(new_row), end(new_row), [&]() {
            return dist(gen);
        });

        return new_row;
    });

    return bitmap;
}

class Image {
public:
    using Bitmap = std::vector<std::vector<uint16_t>>;

    Image() = default;
    Image(Bitmap&& bitmap)
        : bitmap_(std::move(bitmap)) {}

    static Image from1D(std::vector<uint16_t>&& vec) {
        Bitmap bitmap(vec.front());
        std::generate(begin(bitmap), end(bitmap), [&, index{2}, width{vec[1]}]() mutable {
            std::vector<uint16_t> row{vec.begin() + index, vec.begin() + index + width};
            index += width;
            return row;
        });

        return Image(std::move(bitmap));
    }

    const Bitmap& get() const { return bitmap_; }

    void rescale(float factor) {
        if (factor < 1.f) {
            shrink(factor);
            return;
        }
        extend(factor);
    }

    void print() const {
        for (const auto& row : bitmap_) {
            std::copy(cbegin(row), cend(row), std::ostream_iterator<uint16_t>(std::cout, " "));
            std::cout << '\n';
        }
    }

private:
    void shrink(float factor) {
        std::cout << "Shrink Image!\n";
        bitmap_.erase(begin(bitmap_) + bitmap_.size() * factor, end(bitmap_));
        std::transform(begin(bitmap_), end(bitmap_), begin(bitmap_), [factor](auto& row) {
            row.erase(begin(row) + row.size() * factor, end(row));
            return row;
        });
    }

    void extend(float factor) {
        std::cout << "Extend Image!\n";
    }

    Bitmap bitmap_;
};

class Downloader {
public:
    void download(const std::string& url, std::function<void(const std::string& url, Image&& image)> callback) {
        std::cout << "Download in progres:" << std::flush;
        for (int i = 0; i < 20; ++i) {
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            std::cout << "." << std::flush;
        }
        std::cout << "\nImageDownloaded\n";
        callback(url, getBitmap(20, 20));
    }

private:
};

class FileIo {
public:
    // Read whole file if size is equal 0.
    std::vector<uint16_t> read(const std::filesystem::path& path, size_t size = 0) {
        if (!size) {
            std::ifstream file(path);
            std::vector<uint16_t> vec;
            std::copy(std::istream_iterator<uint16_t>(file), std::istream_iterator<uint16_t>{}, std::back_inserter(vec));

            return vec;
        }
        return {};
    }
};

class FileIoFactory {
public:
    static FileIoFactory* get() {
        static FileIoFactory factory;
        return &factory;
    }

    FileIo* instance() {
        if (!fileIo_) {
            fileIo_ = std::make_unique<FileIo>();
        }

        return fileIo_.get();
    }

    void setTestingInstance(std::unique_ptr<FileIo> fileIo) {
        fileIo_ = std::move(fileIo);
    }

private:
    FileIoFactory() = default;

    std::unique_ptr<FileIo> fileIo_;
};

class Printer {
public:
    Printer(std::unique_ptr<Downloader> downloader)
        : downloader_(std::move(downloader)) {}

    void printImage(const std::string& url) {
        if (const auto it = cache_.find(url); it != std::cend(cache_)) {
            std::cout << "Read cached image!\n";
            it->second.print();
            return;
        } else if (std::filesystem::exists(std::filesystem::path(url))) {
            std::cout << "Read image from file!\n";
            cache_[url] = Image::from1D(FileIoFactory::get()->instance()->read(url));
            cache_[url].print();
            return;
        }

        std::cout << "Need to download image!\n";
        downloader_->download(url, [this](const std::string& url, Image&& image) {
            ImageDownloaded(url, std::move(image));
        });
    }

private:
    void ImageDownloaded(const std::string& url, Image&& image) {
        std::cout << "Cllback called!\n\n";
        image.print();
        cache_[url] = std::move(image);
    }

    std::unique_ptr<Downloader> downloader_;
    std::unordered_map<std::string, Image> cache_;
};

void printSeparator() {
    std::cout << "\n\n"
              << std::string(80, '-') << "\n\n";
}

int main() {
    {
        Image img(getBitmap(20, 20));
        img.print();

        std::cout << "\n\nRESCALE\n\n";
        img.rescale(0.5f);
        img.print();
        printSeparator();
    }

    auto downloader = std::make_unique<Downloader>();
    Printer printer(std::move(downloader));
    printer.printImage("some_image.png");
    printSeparator();
    printer.printImage("Hello.png");
    printer.printImage("Hello.png");
}
