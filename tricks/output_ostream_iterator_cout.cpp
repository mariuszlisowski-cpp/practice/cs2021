#include <iostream>
#include <iterator>

int main() {
    std::ostream_iterator<int> output(std::cout, " ");

    for (auto value : { 1, 2, 3, 4 ,5 }) {
        *output++ = value;
    }

    return 0;
}
