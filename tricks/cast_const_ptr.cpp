#include <iostream>

void change_const_ptr(const char* ptr) {                // working on a copy but...
    char* non_const_ptr{ const_cast<char*>(ptr)};       // removing const from a pointer
    *non_const_ptr = 'b';
    /* alternatively...
    *const_cast<char*>(ptr) = 'b'; */
}

int main() {
    char letter{ 'a' };
    const char* const c{ &letter };                     // const ptr pointing to a const value
                                                        // not allowed to modify a pointer nor a value
    std::cout << letter << std::endl;
    change_const_ptr(c);
    std::cout << letter << std::endl;                   // but a pointer HAS BEEN changed

    return 0;
}
