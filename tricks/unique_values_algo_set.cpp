#include <algorithm>
#include <iostream>
#include <iterator>
#include <sstream>
#include <string>
#include <unordered_set>

/* using std::set data structure*/
std::string get_uniques_set(const std::string& word) {
    std::unordered_set<char> uniques{word.begin(), word.end()};                     // set stores unique values only

    std::ostringstream os;
    std::copy(uniques.begin(), uniques.end(), std::ostream_iterator<char>(os));

    return os.str();
}

/* using STL algorithms */
std::string get_uniques_algo(std::string& word) {
    std::sort(word.begin(), word.end());                                            // sort before removing mixed duplicates
    auto first_non_unique = std::unique(word.begin(), word.end());
    word.erase(first_non_unique, word.end());                                       // as 'unique' removes ONLY consecutive values

    return word;
}

int main() {
    std::string word{"abbcccdddd"};

    {
    auto uniques = get_uniques_set(word);
    std::cout << uniques << std::endl;
    }
    {
    auto uniques = get_uniques_algo(word);
    std::cout << uniques << std::endl;
    }

    return 0;
}
