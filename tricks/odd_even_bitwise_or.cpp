#include <iostream>

/* OR Operation of the Number by 1 increment the value of the number by 1 if the number is even
   otherwise it will remain unchanged.
   So, if after OR operation of number with 1 gives a result which is greater than the numbe
   then it is even and we will return true otherwise it is odd and we will return false.
*/

int main() {
    int odd{ 1 };                                   // last bit set
    int even{ 2 };                                  // last bit NOT set

    if (even | 1 > even) {
        std::cout << "> even" << std::endl;
    } else {
        std::cout << "> odd" << std::endl;
    }

    return 0;
}
