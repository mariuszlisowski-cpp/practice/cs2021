#include <mutex>

class ThreadsafeCounter {
    mutable std::mutex m;                           // The "M&M rule": mutable and mutex go together
    int data = 0;

 public:
    int get() const {                               // const getter thus will not modify but...
        std::lock_guard<std::mutex> lk(m);          // able to modify mutex (as it's mutable)
        return data;
    }
    void inc() {
        std::lock_guard<std::mutex> lk(m);
        ++data;
    }
};

int main() {

}
