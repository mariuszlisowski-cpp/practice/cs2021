#include <iostream>

void reference_to_ptr(const char*& ptr) {                  // working on original pointer
    ptr = nullptr;
}

int main() {
    char letter{ 'a' };
    const char* c{ &letter };                              // ptr pointing to a const value
                                                           // not allowed to modify the value

    std::cout << *c << std::endl;
    reference_to_ptr(c);                                   // pointer NULL after returning
    /* std::cout << *c << std::endl;                       // ERROR: dereference nullptr */ 

    return 0;
}
