#include <iostream>

#include <tuple>

int main() {

    std::pair<int, float> pairA{7, 3.14};
    std::pair<int, float> pairB{9, 1.6};

    auto [first, second] = pairA;
    std::cout << first << " : " << second << std::endl;

    std::tie(first, second) = pairB;                            // reuse of structural binding
    std::cout << first << " : " << second << std::endl;

    return 0;
}
