#include <iostream>
#include <fstream>
#include <string>

bool writeFile(const std::string& filename, const std::string& input) {
    std::fstream file(filename, std::ios::out | std::ios::binary);
    if (file.good()) {
        file << input;
        file.close();
        return true;
    } 

    return false;
}

bool readFile(const std::string& filename, std::string& output) {
    std::fstream file(filename, std::ios::in | std::ios::binary);
    if (file.good()) {
        std::copy(std::istreambuf_iterator<char>(file), {},
               // std::istreambuf_iterator<char>(),                         // alternatively
                  std::back_inserter(output));
        file.close();
        return true;
    }

    return false;
}

int main() {
    const std::string filename("sentence");
    std::string input("Hello my beautiful world!");
    std::string output;

    auto is_written = writeFile(filename, input);
    std::cout << (is_written ? "File write success!" : "File write failed!")
              << std::endl;

    auto is_read = readFile(filename, output);
    std::cout << (is_read ? "File read success!" : "File read failed!")
              << std::endl;

    std::cout << output << std::endl;

    return 0;
}
