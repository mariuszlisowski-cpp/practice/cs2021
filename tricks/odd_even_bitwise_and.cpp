#include <iostream>

/* AND Operation of the Number by 1 will be 1, if it is odd because the last bit will be already set
   otherwise it will give 0 as output.
*/

int main() {
    int odd{ 1 };                                   // last bit set
    int even{ 2 };                                  // last bit NOT set

    if (even & 1) {
        std::cout << "> odd" << std::endl;
    } else {
        std::cout << "> even" << std::endl;
    }

    return 0;
}
