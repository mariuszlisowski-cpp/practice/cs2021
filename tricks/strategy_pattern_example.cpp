#include <iostream>
#include <numeric>
#include <vector>

/* strategy pattern */
class Algo {
public:
    virtual ~Algo() = default;

    int operator()(const std::vector<int>& vec) const {
        size_t sum{};
        for (const auto el : vec) {
            if (pred(el)) {                                                 // sum of...
                sum += fun1(el);
                continue;
            }
            sum += fun2(el);
        }
        return sum;
    }

    virtual bool pred(int) const = 0;
    virtual int fun1(int) const = 0;
    virtual int fun2(int) const = 0;
};

/* 1st strategy */
class OddMultipliedByThreeAndFive : public Algo {
public:
    ~OddMultipliedByThreeAndFive() override = default;

    bool pred(int number) const override {
        return number & 1;                                                  // odd
    }
    int fun1(int number) const override {
        return number * 3;

    }
    int fun2(int number) const override {
        return number * 5;
    }
};

/* 2nd strategy */
class EvenMultipliedByTenAndTwo : public Algo {
public:
    ~EvenMultipliedByTenAndTwo() override = default;

    bool pred(int number) const override {
        return !(number & 1);                                               // even
    }
    int fun1(int number) const override {
        return number * 10;

    }
    int fun2(int number) const override {
        return number * 2;
    }
};

int main() {
    std::vector<int> v(50);
    std::iota(begin(v), end(v), 1);

    const Algo& algo1 = OddMultipliedByThreeAndFive();                      // sum of...
    std::cout << algo1(v) << '\n';                                          // result

    const Algo& algo2 = EvenMultipliedByTenAndTwo();
    std::cout << algo2(v) << '\n';

    return 0;
}
