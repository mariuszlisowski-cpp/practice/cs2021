#include <iostream>
#include <map>

template<typename T>
void print(T& container) {
    for (const auto& [key, value] : container) {
        std::cout << key << value << std::endl;
    }
}

int main() {
    std::multimap<char, int> mm;
    mm.insert({ 'a', 1 });
    mm.insert({ 'a', 2 });
    mm.insert({ 'a', 3 });
    mm.insert({ 'b', 4 });
    print(mm);

    /* first found */
    auto it = mm.find('a');
    for (; it != mm.end(); ++it) {
        std::cout << it->first << " | " << it->second << std::endl;
    }

    /* equal range */
    auto pair_it = mm.equal_range('a');                                 // [first, second)
    for (auto it = pair_it.first; it != pair_it.second; ++it) {
        std::cout << it->first<< " | " << it->second << '\n';
    }
    std::cout << pair_it.first->second << " - "
              << pair_it.second->second << std::endl;                   // second is excluded

    return 0;
}
