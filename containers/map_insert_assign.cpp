#include <iostream>
#include <map>

template<typename T>
void print(T& container) {
    for (const auto& [key, value] : container) {
        std::cout << key << value << std::endl;
    }
}

int main() {
    std::map<char, int> m;

    auto it = m.begin();
    m.insert_or_assign(it, 'b', 2);                             // inserts
    m.insert_or_assign(it, 'b', 3);                             // assigns (as already exists)
    print(m);

    m['a'] = 1;                                                 // inserts
    m['a'] = 2;                                                 // assigns (or inserts if )
    print(m);

    return 0;
}
