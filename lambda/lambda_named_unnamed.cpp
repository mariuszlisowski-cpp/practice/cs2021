#include <iostream>

int main() {
    /* named lambda */
    auto isOdd = [](int number) { return number % 2; };             // create
    auto resultA = isOdd(101);                                      // call

    /* unnamed lambda */
    auto resultB = [](int number) { return number % 2; }(101);      // create & call

    std::cout << (resultA ? "odd" : "even") << std::endl;
    std::cout << (resultB ? "odd" : "even") << std::endl;

    /*  */
    []() {};                                                        // empty lambda
    [] {}();                                                        // empty lambda call
    []() {}();                                                      // empty lambda call

    []<int>() {};                                                   // empty parametrized lambda
    (+[]() {})();                                                   // call


    return 0;
}
