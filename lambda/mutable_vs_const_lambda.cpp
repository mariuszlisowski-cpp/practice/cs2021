#include <iostream>

int main() {
    int number{ 1 };
    [number]() mutable { number++; }();                     // capture by value (incremented locally)
    std::cout << number << std::endl;                       // still the same value

    [&number]() { number++; }();                            // capture by reference (no mutable needed)
    std::cout << number << std::endl;                       // value changed

    return 0;
}
