#include <memory>

/* templated funtion */
template <typename T>
std::unique_ptr<T> wrapWithUniquePtr(T value) {
    return std::make_unique<T>(value);
}


int main() {
    /* using templated funtion above */
    auto u_ptrA = wrapWithUniquePtr(4);
    auto u_ptrB = wrapWithUniquePtr(5.3);


    /* saa but with generic lambda */ {                                 // generic lambda accepting 'auto' type
        auto wrapWithUniquePtr = [](auto value) {                       // and unfolding into a templated class
            return std::make_unique<decltype(value)>(value);            // type deduced from 'value' variable
        };

        auto u_ptrA = wrapWithUniquePtr(4);
        auto u_ptrB = wrapWithUniquePtr(5.3);
    }

    return 0;
}
