#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <vector>

/* generic joined lamndas */
template<typename FUN1, typename FUN2, typename PRED>
auto combine(FUN1 funA,
             FUN2 funB,
             PRED logic_fun)
{
    return [=](auto arg) {
        return logic_fun(funA(arg), funB(arg));
    };
}

/* generic grep */
template<typename T, typename CONTAINER, typename FUN>
void grep(CONTAINER cont, FUN fun) {
    std::copy_if(begin(cont),
                 end(cont),
                 std::ostream_iterator<T>(std::cout, " "),
                 fun);
}

int main() {
    /* using generic joined lambdas */
    std::vector<std::string> s{"Ala", "Kot", "Ola", "Nic", "Too long"};
    std::copy_if(begin(s),
                 end(s),
                 std::ostream_iterator<std::string>(std::cout, " "),
                 combine([](const std::string& str) { return str.size() == 3; },
                         [](const std::string& str) { return str.front() == 'A'; },
                         std::logical_or<>{}));
    std::cout << std::endl;

    /* using generic grep */
     auto predicate = combine([](const std::string& str) { return str.size() == 3; },
                              [](const std::string& str) { return str.front() == 'A'; },
                              std::logical_or<>{});
    grep<std::string>(s, predicate);

    return 0;
}
