#include <iostream>

int main() {
    double number = 0.5;
    [&number]() { number += 1.0; }();                               // operator() const

    /* unfolded lambda */
    class __lambda_6_3 {
    public: 
        __lambda_6_3(double & _number)
        : number{_number} {}

        inline void operator()() const {                            // operator const
            number += 1.0;
        }
        
    private: 
        double& number;                                             // reference
        
        public:
        
    } __lambda_6_3{number};

    __lambda_6_3.operator()();                                      // call unfolded lambda

    std::cout << number << std::endl;

    return 0;
}
