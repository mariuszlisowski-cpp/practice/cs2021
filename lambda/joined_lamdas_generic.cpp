#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <numeric>
#include <vector>

/* generic joined lamndas */
template<typename FUN1, typename FUN2, typename PRED>
auto combine(FUN1 funA,                                                         // 1st function
             FUN2 funB,                                                         // 2nd function
             PRED logic_fun)
{
    return [=](auto arg) {
        return logic_fun(funA(arg), funB(arg));
    };
}

int main() {
    /* defining predicates */
    auto is_diveided_by_five = [](int number) {                                 // sygnature: bool(int)
        return number % 5 == 0;
    };
    auto is_odd = [](int number) {                                              // sygnature: bool(int)
        return number & 1;
    };
    auto predicate = combine(is_diveided_by_five,                               // sygnature: bool(bool, bool)
                             is_odd,
                             std::logical_and<>{});                             // bool type deducted

    /* using predicates */
    std::vector<int> v(50);
    std::iota(begin(v), end(v), 0);
    std::copy_if(begin(v),                                                      // 1st function
                 end(v),                                                        // 2nd function
                 std::ostream_iterator<int>(std::cout, " "),
                 predicate);                                                    // precicate
    std::cout << std::endl;

    /* using generic joined lamdas */
    std::vector<char> c{ 'a', 'n', 'N', 'y', 'Y', 'x'};                         // choose N Y only
    std::copy_if(begin(c),                                                      // 1st function
                 end(c),                                                        // 2nd function
                 std::ostream_iterator<char>(std::cout, " "),
                 combine([](char ch) { return std::isupper(ch); },              // precicate
                         [](char ch) { return ch == 'Y' || ch == 'N';  },
                         std::logical_and<>{}));
    std::cout << std::endl;

    return 0;
}
