#include <algorithm>
#include <chrono>
#include <iostream>
#include <numeric>
#include <random>
#include <vector>

template<typename NUMBER>
struct AbsoluteCompare {
    bool operator()(NUMBER lhs, NUMBER rhs) {
        return std::abs(lhs) < std::abs(rhs);
    }
};

template<typename NUMBER>
bool absolute_compare(NUMBER lhs, NUMBER rhs) {
    return std::abs(lhs) < std::abs(rhs);
}

template<typename T>
void print(const std::vector<T>& v) {
    for(auto& el : v) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::vector<int> v(18);
    std::iota(begin(v), end(v), -9);
    std::shuffle(begin(v), end(v),
                 std::mt19937(static_cast<unsigned int>(std::chrono::system_clock::now()
                                                        .time_since_epoch()
                                                        .count())));
    print(v);

    std::sort(begin(v), end(v), AbsoluteCompare<decltype(v)::value_type>{});        // functor (specialized)
                                                                                    // object created by curly braces
    std::sort(begin(v), end(v), absolute_compare<decltype(v)::value_type>);         // function (specialized)
                                                                                    // passed as an address

    auto absolute_compare = [](auto lhs, auto rhs) {                                // named lambda
        return std::abs(lhs) < std::abs(rhs);
    };
    std::sort(begin(v), end(v), absolute_compare);
    std::sort(begin(v), end(v),
              [](auto lhs, auto rhs) {                                              // unnamed lambda
                  return std::abs(lhs) < std::abs(rhs);
              });
    print(v);

    return 0;
}
