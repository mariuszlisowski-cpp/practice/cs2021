#include <iostream>

/* unfolded generic lambda */
struct UnnamedClosureClass {
    template <typename T1, typename T2>
    auto operator()(T1 x, T2 y) const {
        return x + y;
    }
};

int main() {
    /* generic lambda */
    auto add_numbers = [](auto x, auto y) { return x + y; };

    /* saa */
    auto add_numbers_unfold = UnnamedClosureClass{};

    std::cout << add_numbers(2, 3.14) << '\n';
    std::cout << add_numbers_unfold(2, 3.14) << '\n';

    return 0;
}
