#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <numeric>
#include <vector>

/* join lamndas */
auto combine(std::function<bool(int)> funA,
             std::function<bool(int)> funB,
             std::function<bool(bool, bool)> logic_fun)
{
    return [=](int arg) {
        return logic_fun(funA(arg), funB(arg));
    };
}

int main() {
    auto is_diveided_by_five = [](int number) {                     // sygnature: bool(int)
        return number % 5 == 0;
    };
    auto is_odd = [](int number) {                                  // sygnature: bool(int)
        return number & 1;
    };

    auto predicate = combine(is_diveided_by_five,                   // sygnature: bool(bool, bool)
                             is_odd,
                             std::logical_and<>{});                 // bool type deducted

    std::vector<int> v(50);
    std::iota(begin(v), end(v), 0);

    std::copy_if(begin(v),
                 end(v),
                 std::ostream_iterator<int>(std::cout, " "),
                 predicate);
    
    /* console input */
    // std::copy_if(std::istream_iterator<int>(std::cin),
    //              {},
    //              std::ostream_iterator<int>(std::cout, " "),
    //              predicate);
    

    return 0;
}
