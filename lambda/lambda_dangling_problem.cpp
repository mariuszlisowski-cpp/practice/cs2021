#include <iostream>

auto getIndexGenerator() {
    int value = 0;
    auto lambda = [&value]() mutable {                      // captured by reference
        return value++;
    };

    return lambda;                                          // dangling reference (out of scope)
}

int main() {
    auto generator = getIndexGenerator();                   // labmda returned (value initialized with zero)
    
    for (int i = 0; i < 5; ++i) {
        std::cout << generator() << '\n';                   // undefined behaviour (UB)
    }
    
    return 0;
}
