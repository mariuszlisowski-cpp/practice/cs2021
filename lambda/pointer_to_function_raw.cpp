/* pointer to the function examples:
    int(*f)()                   - f takes no arguments and returns and int
    void(*f)(int)               - f takes an int and returns nothing
    double(*f)(int, string)     - f takes an int and a string and returns double
    
    raw vs functional:
    std::function<double(int, string)> f
                  double(*f)(int, string)
                        ^^^^                // removed
 */
#include <iostream>

/* accepts a pointer to the function */
template<typename T>
auto foo( T(*function_ptr)()) {
    return function_ptr();                                  // returns T type
}

int main() {
    auto lambdaA = [] { return 42; };
    std::cout<< foo<int>(lambdaA);

    auto lambdaB = []() { return true; };
    std::cout<< std::boolalpha
             << foo<bool>(lambdaB);

    int value{};
    auto lambdaC = [value] { return 42; };                  // captured value
    // std::cout<< foo<int>(lambdaC);                       // cannot convert  '<lambda()>' to type 'int (*)()'

    return 0;
}
