#include <iostream>

int main() {
    auto isNotNullptr = [](void* ptr) -> bool {                 // returns bool but...
                            return ptr;                         // would return pointer to void
                        };

    int* ptr;
    std::cout << (isNotNullptr(ptr) ? "null " : "not null ");
    std::cout << (ptr ? "null " : "not null ");                 // but shorter also works

    return 0;
}
