#include <iostream>

int main() {
    /* generic lambda */
    auto multiplyByFactor = [factor = 10](auto number) {            // accepting any number
        return number * factor;
    };

    std::cout << multiplyByFactor(20) << '\n';
    std::cout << multiplyByFactor(1.23) << '\n';

    return 0;
}
