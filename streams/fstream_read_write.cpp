#include <algorithm>
#include <iostream>
#include <fstream>

int main() {
    const char* filename{ "fstream_read_write.cpp" };

    /* write */ {
    std::fstream source_write(filename,
                               source_write.out | source_write.app);    // write, append (at the end)

    if (source_write) {
        source_write << "// comment added\n";
        // source_write.close();                                        // no need as opened in c'tor (RAII)
    }
    } // write                                                          // RAII in action (fstream d'tor called)

    /* read */ {
    std::fstream source_read(filename, std::ios::in);                   // read mode (default read & write)
    if (source_read) {
        // read by word
        std::string word;
        while (source_read >> word) {                                   // until EOF (deliminter is whitespace)
            std::cout << word << ' ';
        }
        std::cout << "\n\n";
        
        // reset stream
        source_read.clear();                                            // clear EOF
        source_read.seekg(std::ios::beg);                               // set at the begining

        // read by line
        std::string line;
        while (!std::getline(source_read, line, '\n').eof()) {          // until EOF
            std::cout << line << std::endl;
        }
    }
    } // read                                                           // fstream d'tor called

    return 0;
}
