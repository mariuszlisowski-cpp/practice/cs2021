#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <map>
#include <string>

namespace std {
std::ostream& operator<<(std::ostream& os, const std::pair<std::string, std::string>& p) {
    return os << std::quoted(p.first) << " means " << std::quoted(p.second) << " in polish";
}
}

int main() {
    std::map<std::string, std::string> dict{
        { "dog", "pies" },
        { "cat", "kot" },
        { "lion", "lew" }        
    };

    std::copy(begin(dict), end(dict),
              std::ostream_iterator<decltype(dict)::value_type>(std::cout, "\n"));

    return 0;
}
