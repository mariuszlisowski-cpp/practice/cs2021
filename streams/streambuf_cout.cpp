#define _USE_MATH_DEFINES                               // g++ -D_USE_MATH_DEFINES
#include <cmath>
#include <iomanip>
#include <iostream>

int main() {
    /* share std::cout */
    std::ostream os(std::cout.rdbuf());
    /* alternatively
    std::streambuf* sbuf = std::cout.rdbuf();
    std::ostream os(sbuf); */

    /* set different style of stream */
    os << std::setprecision(10);
    os << M_PI << std::endl;                            // same as cout

    /* use default style */
    std::cout << M_PI << std::endl;

    return 0;
}
