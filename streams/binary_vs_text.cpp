#include <fstream>
#include <iostream>

int main() {
    /* text write */ {
    std::ofstream file("file_text");                                    // overwrite mode (no append), RAII

    file << "Hello world\n";                                            // higher abstraction (for strings)
    file << 3.1434455445 << '\n';                                       // precission cut
    file << 42 << '\n';
    }                                                                   // file closed

    /* binary write */ {
    std::ofstream file("file_binary", std::ios::binary);                // overwrite mode (no append), RAII

    std::string s{ "Hello world" };
    file.write(s.c_str(), s.size());                                    // low level write (fo binaries)
    }

    return 0;
}
