#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>

int main() {
    std::stringstream ss;

    ss << "Hello\n";                                                            // easy append
    ss << 123 << '\n';
    ss << std::setprecision(10);
    ss << 3.1456544857 << '\n';

    std::cout << ss.str() << std::endl;                                         // convert to string

    return 0;
}
