#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <map>
#include <string>

int main() {
    std::string polish_filename{ "polish.txt" };
    std::string english_filename{ "english.txt" };
    std::string pol_eng_filename{ "pol-eng.txt" };

    std::ifstream polish(polish_filename);
    std::ifstream english(english_filename);
    std::ofstream pol_eng(pol_eng_filename);                                // out file

    std::map<std::string, std::string> dict;
    std::transform(std::istream_iterator<std::string>(polish), {},
                   std::istream_iterator<std::string>(english),
                   std::ostream_iterator<std::string>(pol_eng, "\n"),       // file out (or std::cout)
                   [](const std::string& polish_word,
                      const std::string& english_word)
                   {
                       std::stringstream ss;
                       ss << std::quoted(polish_word) << " - " 
                          << std::quoted(english_word);
                       return ss.str();
                   });

    return 0;
}
