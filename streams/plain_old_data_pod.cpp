/* pod - no c'tor, d'tor & virtual methods */

#include <iostream>
#include <fstream>
#include <string>

#include <string.h>                                                         // for strncpy()

class POD {
public:
    // setters
    void set_index(int index) {
        index_ = index;
    }
    void set_average(double average) {
        average_ = average;
    }
    void set_name(const std::string& name) {
        strncpy(name_, name.data(), 15);
    }

    // getters
    size_t get_index() {
        return index_;
    }
    double get_average() {
        return average_;
    }
    std::string get_name() {
        return name_;                                                       // implicit conversion to string
    }

private:                                                                    // build-in datatypes
    size_t index_;                                                          // 4 bytes
    double average_;                                                        // 8 bytes
    char name_[15];                                                         // 15 bytes
    // std::string name_;                                                   // no string (has c'tor)
};

int main() {
    POD pod;
    pod.set_index(123);
    pod.set_average(4.24);
    pod.set_name("any pod");

    /* write pod */ {
    std::fstream pod_stream("pod", std::ios::binary | std::ios::out);
    if (pod_stream) {
        pod_stream.write(reinterpret_cast<char*>(&pod), sizeof(POD));       // cast allowed here (no checks)
    }
    }

    POD pod_new;                                                            // to be filled
    
    /* read pod */ {
    std::fstream pod_stream("pod", std::ios::binary | std::ios::in);
    if (pod_stream) {
        pod_stream.read(reinterpret_cast<char*>(&pod_new), sizeof(POD));    // filled
    }
    }

    /* checks */
    std::cout << pod_new.get_index() << std::endl;
    std::cout << pod_new.get_average() << std::endl;
    std::cout << pod_new.get_name() << std::endl;

    return 0;
}
