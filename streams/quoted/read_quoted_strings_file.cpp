#include <fstream>
#include <iomanip>                                                              // quoted
#include <iostream>
#include <string>


int main() {
    std::string filename("quoted_strings.txt");
    std::ifstream quoted_file(filename);

    /* file format:
       "any nameA" "any nickA" "any hobby"                                      // no need for newline (ignored)
       "any nameB" "any nickB" "any hobby"                                      // reads newline inside quotes !
    */                   
    std::string name;
    std::string nick;
    std::string hobby;
    while (quoted_file >> std::quoted(name)
                       >> std::quoted(nick)
                       >> std::quoted(hobby))
    {
        std::cout << name << " / " << nick << " / " << hobby << std::endl;
    }

}
