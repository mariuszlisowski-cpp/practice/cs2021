#include <memory>
#include <ostream>
#include <iostream>
#include <string>
#include <vector>

struct Coords
{
    double latitude;
    double longitude;
};

class City
{
public:
    City(const std::string& name, const Coords& coords)
        : name(name)
        , coords(coords) {}

    friend std::ostream& operator<<(std::ostream& os, const City& city);

private:
    std::string name;
    Coords coords;
};

std::ostream& operator<<(std::ostream& os, const City& city) {
    os << "City:\t\t" << city.name << '\n'
        << "Latitude:\t" << city.coords.latitude << '\n'
        << "Longitude:\t" << city.coords.longitude << '\n';

    return os;
}

template<typename T>
class Pointer
{
public:
    explicit Pointer(T obj) {
        ptr = std::make_shared<T>(obj);
    }
    /* rule of zero */
    // Pointer(const Pointer& other) = default;                         // not needed
    // Pointer(Pointer&& other) = default;                              // saa
    // Pointer& operator=(const Pointer& other) = default;              // saa
    // Pointer& operator=(Pointer&& other) = default;                   // saa
    // ~Pointer() = default;                                            // saa
    virtual ~Pointer() = default;                                       // for polymorphism

    T get() {
        return *ptr;
    }

private:
    std::shared_ptr<T> ptr;
};

int main() {
    Pointer<City> cityA(City("Sosnowiec", {50.286263, 19.104078}));
    Pointer<City> cityB(City("Katowice", {50.270908, 19.039993}));

    Pointer<City> cityC(cityA);                                         // copy c'tor
    std::cout << cityC.get() << std::endl;
    
    cityC = cityB;                                                      // copy assignment
    std::cout << cityC.get() << std::endl;

    cityC = std::move(cityA);                                           // move assignment
    std::cout << cityC.get() << std::endl;

    Pointer<City> cityD(std::move(cityC));                              // move c'tor
    std::cout << cityD.get() << std::endl;
}
