#include <iostream>

class Bar {
public:
    // setter & getter (insufficient)
    int& number() { return number_; }

    // setters & getters (correct API)
    int& value() & { return value_; }                                   // setter/getter for l-values
    const int& value() const & { return value_; }                       // getter for const l-values

    int value() && { return std::move(value_); }                        // getter for r-values
    const int&& value() const  && { return std::move(value_); }         // getter for const r-values

private:
    int number_{};
    int value_{};
};

int main() {
    /* non-const */
    Bar barA;
    barA.number() = Bar{}.number();                                     // undefined behaviour

    barA.value() = 24;                                                  // use setter for l-value
    std::cout << barA.value() << std::endl;                             // use getter for l-value

    /* const */
    const Bar barB{};
    std::cout << barB.value() << std::endl;                             // use getter for const l-value
    // barB.value() = 42;                                               // no setter for const l-value (as const)

    int new_bar_owner = Bar{}.value();                                  // use getter for r-value
    const int && new_const_bar_owner = Bar{}.value();                   // use setter for const r-value

    return 0;
}
