#include <iostream>

class Clazz {};

void f(const Clazz&)     { std::cout << "const Clazz&\n"; }
void f(Clazz&)           { std::cout << "Clazz&\n"; }
void f(Clazz&&)          { std::cout << "Clazz&&\n"; }

/* universal reference */
template<typename T>                                                    // universal as a template
void use(T&& g) {
    f(std::forward<T>(g));                                              // template type mandatory
}                                                                       // forward use only in templates

int main() {
    const Clazz cg;
    Clazz g;

    use(cg);                                                            // const l-value
    use(g);                                                             // l-value
    use(Clazz());                                                       // r-value

    return 0;
}
