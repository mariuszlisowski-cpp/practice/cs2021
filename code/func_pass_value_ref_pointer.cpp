#include <iostream>

void cpy(int a) {                                       // pass by value
    ++a;
};

void ref(int& a) {                                      // pass by reference
    ++a;
}

void ptr(int* a) {                                      // pass by pointer
    ++*a;
}

int main() {
    int c{};                                            // initial zero
    int& r = c;
    int* p = &c;

    cpy(c);
    std::cout << c << std::endl;                        // not incremented

    ref(r);
    std::cout << c << std::endl;                        // incremented

    ptr(p);
    std::cout << c << std::endl;                        // incremented

    /* usage */
    cpy(c);
    cpy(r);
    cpy(*p);                                            // dereference operator

    ref(c);
    ref(r);
    ref(*p);                                            // dereference operator

    ptr(&c);                                            // pass address of value
    ptr(&r);                                            // pass address of reference
    ptr(p);                                             // pass pointer

    return 0;
}
