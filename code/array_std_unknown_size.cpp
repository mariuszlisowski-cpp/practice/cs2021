#include <array>
#include <iostream>

template<typename T, std::size_t SIZE>
void print(std::array<T, SIZE>& arr) {
    for (auto& el : arr) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::array<int, 10> ones;
    ones.fill(1);
    ones[3] = 3;

    std::array<int, 10> twos;
    twos.fill(2.2);
    twos.swap(ones);

    print(ones);
    print(twos);
    
    auto a = std::array<float, 2>{ { 1.1, 2.2 } };
    /* alternatively
    std::array<float, 2> a{ { 1.1, 2.2 } }; */
    print(a);

    return 0;
}
