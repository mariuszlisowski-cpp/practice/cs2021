#include <iostream>

// weak enum type
enum Color {
    RED,
    GREEN,
    BLUE
};

// strong enum type
enum class TrafficLight {
    RED,
    GREEN,
    YELLOW
};

int main() {
    auto color{ Color::RED };
    auto light{ TrafficLight::RED };

    // regular enum
    if (color == RED) {
        std::puts("Red color");
    }

    // enum class
    if (light == TrafficLight::RED) {
        std::puts("Stop!");
    }

    return 0;
}
