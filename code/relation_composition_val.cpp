/* Composition - OWNS relationship 
   direct containment - by value
*/
#include <iostream>

class Room {
public:
    void livingroom() {
        std::cout << "> lama in the livingroom\n";
    };
};

class House {
public:
    Room bedroom;                           // direct containment
};                                          // house OWNS the room and it is
                                            // destroyed when the house is
int main() {
    {
        House house;
        house.bedroom.livingroom();
    }                                       // house and room destruction

    return 0;
}
