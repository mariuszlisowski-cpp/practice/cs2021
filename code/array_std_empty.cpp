#include <array>
#include <iostream>
#include <iterator>

int main() {
    /* whether array is empty */
    std::array<int, 3> other;           // uninitialized values
    other.fill(0);
    if (!other.empty()) {
        std::cout << "> array is NOT empty as its size is " << other.size() <<  std::endl;
    }

    std::copy(other.cbegin(), other.cend(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << '\n';

    std::array<int, 0> empty;
    if (empty.empty()) {
        std::cout << "> array is empty as its size is " << empty.size() << std::endl;
    }

    return 0;
}
