#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };

    v.erase(std::remove_if(begin(v), end(v),
                   [](auto el) {
                       return el % 2;                       // true if odd
                       // return el & 1;                    // true if odd
                   }),
            v.end());
    
    for (auto el : v) {
        std::cout << el << std::endl;
    }

    return 0;
}
