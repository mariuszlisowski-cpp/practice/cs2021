/* passing a function to a function */
#include <functional>
#include <iostream>
#include <vector>

// using a template
template<typename T>
std::vector<int> functA(int max, T f) {
    std::vector<int> out;
    for (size_t i = 0; i <= max; ++i) {
        if (f(i)) {
            out.push_back(i);
        }
    };

    return out;
}

// using a functional header
std::vector<int> functB(int max, std::function<bool(int)> f) {
    std::vector<int> out;
    for (size_t i = 0; i <= max; ++i) {
        if (f(i)) {
            out.push_back(i);
        }
    };

    return out;
}

int main() {
    /* divided by */
    for (auto&& el : functB(12, [](auto i) { return i % 3 == 0; })) {
        std::cout << el << std::endl;
    }

    return 0;
}
