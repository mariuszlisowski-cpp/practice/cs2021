#include <memory>
#include <iostream>

void foo(const std::shared_ptr<int> ptr) {
    *ptr = 20;
    std::cout << "> value: " << *ptr << std::endl;
    std::cout << "> inside foo:  " << ptr.use_count() << std::endl;
}

int main() {
    std::shared_ptr<int> number{ std::make_shared<int>(7)};
    
    std::cout << "> outside foo: " << number.use_count() << std::endl;
    foo(number);
    std::cout << "> outside foo: " << number.use_count() << std::endl;

    return 0;
}
