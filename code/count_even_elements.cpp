#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    //            even:    *     *     *     *

    size_t counter{};
    std::for_each(begin(v), end(v),
                  [&counter](auto el) {
                      if (!(el & 1)) {
                          ++counter;
                      }
                  });

    std::cout << "number of even elements: " << counter << std::endl;

    return 0;
}
