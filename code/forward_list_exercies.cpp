#include <iostream>
#include <forward_list>

template<typename T>
void print(const T& l) {
    for (auto el : l ) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::forward_list<int> list{ 0, 1, 2, 3, 4, 5 };
    print(list);

    list.erase_after(list.before_begin());                  // first element
    print(list);
    
    list.erase_after(list.begin());                         // second element
    print(list);

    list.erase_after(std::next(list.begin(), 2));           // fourth element
    print(list);

    list.insert_after(std::next(list.begin(), 2), 77);      // add fourth
    print(list);

    return 0;
}
