#include <iostream>
#include <deque>

template<typename T>
void print(const T& l) {
    for (auto el : l ) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::deque<int> d{ 1, 2, 3, 4, 5 };
    print(d);

    d.erase(d.begin() + 1);
    d.erase(d.begin() + 2);
    print(d);

    d.push_front(77);
    d.push_back(99);
    print(d);

    d.insert(d.begin() + 3, -1);
    print(d);

    return 0;
}
