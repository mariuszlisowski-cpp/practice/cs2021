/* linearithmic time complexity 

   O(1)          constant
   O(log n)      logarithmic    
   O(n)          linear
-> O(n log n)    linearithmic
   O(n^c)        polynomial
   O(c^n)        exponential      
   O(n!)         factorial   */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <iterator>
#include <vector>
#include <random>

int main() {
    constexpr size_t samples = 10'000'000;
    std::vector<int> vec(samples);
    
    /* fill vector */
    std::iota(begin(vec), end(vec), 0);
    
    /* shuffle randomly */
    std::random_device rd;
    std::mt19937 gen(rd());
    std::shuffle(begin(vec), end(vec), gen);

    /* sort */
    auto start = std::chrono::high_resolution_clock::now();
    std::sort(begin(vec), end(vec));
    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << "O(nlogn): "
              << std::chrono::duration_cast<std::chrono::seconds>(stop - start).count() << " s\n";

    return 0;
}
