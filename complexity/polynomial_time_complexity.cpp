/* polynomial time complexity 
   
   O(1)          constant
   O(log n)      logarithmic    
   O(n)          linear
   O(n log n)    linearithmic
-> O(n^c)        polynomial
   O(c^n)        exponential      
   O(n!)         factorial   */

#include <algorithm>
#include <chrono>
#include <functional>
#include <iostream>
#include <iterator>
#include <random>
#include <vector>

/* double loop thus O(n^2) */
void bubble_sort(std::vector<int>& v) {
    bool is_swapped;
    int v_size = v.size();
    do {                                                                    // first loop
        is_swapped = false;
        for (int i = 0; i < v_size - 1; ++i) {                              // second loop
            if (v[i] > v[i + 1]) {
                std::swap(v[i], v[i + 1]);
                is_swapped = true;                                          // swap occured so not finished yet
            }
        }                                                                   // last element is the biggest
        --v_size;                                                           // so can now be omitted
    } while (is_swapped);                                                   // no more swaps so finished
}

void fill_vector_randomly(std::vector<int>& vec, int min, int max) {
    std::mt19937 mt(std::random_device{}());
    std::uniform_int_distribution uid(min, max);
    std::generate(begin(vec), end(vec),
                  std::bind(uid, std::ref(mt)));
}

void measure_sorting_time(std::vector<int>& v) {
    auto start = std::chrono::high_resolution_clock::now();
    bubble_sort(v);
    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << "O(n^2): "
              << std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count()
              << " ms" << std::endl;
}

int main() {
    /* special case of polynomial: quadratic 
       occurs in bubble sort */

    constexpr size_t samples1 = 1'000;
    std::vector<int> v1(samples1);
    fill_vector_randomly(v1, 1, 9);
    measure_sorting_time(v1);

    constexpr size_t samples2 = 10'000;                                     // size is 10 times bigger...
    std::vector<int> v2(samples2);
    fill_vector_randomly(v2, 1, 9);
    measure_sorting_time(v2);                                               // so 100 times slower (10^2)

    constexpr size_t samples3 = 100'000;                                    // size is 100 times bigger...
    std::vector<int> v3(samples3);
    fill_vector_randomly(v3, 1, 9);
    measure_sorting_time(v3);                                               // so 10'000 times slower (100^2)
                                                                            // be patient!
    return 0;
}
