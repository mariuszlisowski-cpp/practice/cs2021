/* linear-factorial time complexity 

   O(1)          constant
   O(log n)      logarithmic    
   O(n)          linear
   O(n log n)    linearithmic
   O(n^c)        polynomial
   O(c^n)        exponential      
   O(n!)         factorial   
-> O(n n!)       linear-factorial*/

#include <algorithm>
#include <chrono>
#include <iostream>
#include <iterator>
#include <random>
#include <vector>

std::random_device rd;
std::mt19937 generator(rd());

// joke sort
void bogoSort(std::vector<int>& vec) {
    while (!std::is_sorted(vec.begin(), vec.end())) {                                       // shuffle if not sorted
        std::shuffle(vec.begin(), vec.end(), generator);
    }
}

int main() {
    constexpr size_t samples = 11;                                                          // only 11 elements! 
                                                                                            // do not try for 20
    std::vector<int> vec(samples);
    std::iota(begin(vec), end(vec), 0);
    std::shuffle(vec.begin(), vec.end(), generator);
    auto start = std::chrono::high_resolution_clock::now();
    bogoSort(vec);
    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << "O(n * n!): "
              << std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count()
              << " ms\n";

    return 0;
}
