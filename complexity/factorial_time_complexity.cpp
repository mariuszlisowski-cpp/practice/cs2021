/* factorial time complexity 

   O(1)          constant
   O(log n)      logarithmic    
   O(n)          linear
   O(n log n)    linearithmic
   O(n^c)        polynomial
   O(c^n)        exponential      
-> O(n!)         factorial   */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

/* caclulated during compilation thus fast */
constexpr unsigned long long factorial(int n) {
    return n == 0 
           ? 1 
           : n * factorial(n - 1);
}

int main() {
    std::cout << factorial(59);

    /* examples:
       Travelling Salesman Problem (TSP) */

    return 0;
}
