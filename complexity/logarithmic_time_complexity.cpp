/* logarithmic time complexity 

   O(1)          constant
-> O(log n)      logarithmic    
   O(n)          linear
   O(n log n)    linearithmic
   O(n^c)        polynomial
   O(c^n)        exponential      
   O(n!)         factorial   */

#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template<typename T>
auto binary_search(const std::vector<T>& v, int find_me) {
    auto lhs{ v.begin() };
    auto rhs{ v.end() - 1};
    while (lhs <= rhs) {
        auto mid = lhs + (std::distance(lhs, rhs) / 2);
        if (find_me == *mid) {
            return mid;
        }
        if (find_me < *mid) {
            rhs = mid - 1;
        } else {
            lhs = mid + 1;
        }
    }

    return v.end();
}

int main() {
    std::vector<int> vec{1, 2, 3, 4, 5, 6};

    /* binary search implements O(log n) complexity */
    std::cout << *binary_search(vec, 6) << std::endl;                           // iterator dereference

    // STL version
    std::cout << std::boolalpha
              << std::binary_search(begin(vec), end(vec), 2) << std::endl       // value present
              << std::binary_search(begin(vec), end(vec), 0) << std::endl;      // value absent

    return 0;
}
