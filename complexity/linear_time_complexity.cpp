/* linear time complexity 

   O(1)          constant
   O(log n)      logarithmic    
-> O(n)          linear
   O(n log n)    linearithmic
   O(n^c)        polynomial
   O(c^n)        exponential      
   O(n!)         factorial   */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <numeric>
#include <vector>

int main() {
    constexpr size_t samples = 100'000'000;
    constexpr size_t search_num = 99'654'321;                                  // to be found
    
    std::vector<int> vec(samples);
    std::iota(begin(vec), end(vec), 0);

    /* linear O(n) */
    for (const auto el : vec) {
        if (el == search_num) {
            std::cout << "found!" << std::endl;
            break;
        }
    }

    return 0;
}
