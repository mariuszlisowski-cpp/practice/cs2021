/* logarithmic vs linear time complexity 

   O(1)          constant
-> O(log n)      logarithmic    
-> O(n)          linear
   O(n log n)    linearithmic
   O(n^c)        polynomial
   O(c^n)        exponential      
   O(n!)         factorial   */

#include <algorithm>
#include <chrono>
#include <iostream>
#include <numeric>
#include <vector>

int main() {
    constexpr size_t samples = 100'000'000;
    constexpr size_t search_num = 99'654'321;                                  // to be found
    
    std::vector<int> vec(samples);
    std::iota(begin(vec), end(vec), 0);

    /* logarithmic O(log n) */
    auto start = std::chrono::high_resolution_clock::now();
    std::binary_search(begin(vec), end(vec), search_num);
    auto stop = std::chrono::high_resolution_clock::now();
    std::cout << "O(logn): "
              << std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count() << " ns\n";

    /* linear O(n) */
    start = std::chrono::high_resolution_clock::now();
    for (const auto el : vec) {
        if (el == search_num) {
            break;
        }
    }
    stop = std::chrono::high_resolution_clock::now();
    std::cout << "O(n):    "
              << std::chrono::duration_cast<std::chrono::nanoseconds>(stop - start).count() << " ns\n";

    return 0;
}
