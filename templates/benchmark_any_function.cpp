#include <chrono>
#include <functional>
#include <iostream>
#include <thread>
#include <type_traits>

using namespace std::chrono_literals;
using std::chrono::milliseconds;

template<typename T, typename U>
std::common_type_t<T, U> add(const T& lhs, const U& rhs) {
    std::this_thread::sleep_for(200ms);
    return lhs + rhs;
}

template<typename Clock, typename Fun, typename... Args>
auto benchmark(Fun fun, Args&&... args) {
    const auto start = Clock::now();
    std::invoke(std::move(fun), std::forward<Args>(args)...);
    const auto end = Clock::now();

    return std::chrono::duration<double, std::milli>(end - start);
}

int main() {
    auto result = benchmark<std::chrono::system_clock>(add<float, int>, 3.14, 2);

    std::cout << result.count() << std::endl;

    return 0;
}
