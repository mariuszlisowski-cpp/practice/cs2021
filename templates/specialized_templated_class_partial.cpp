#include <iostream>

/* primary template */
template <typename T1, typename T2, typename T3>
struct Foo {
    void print() { std::cout << "primary\n"; }
};

/* #1a partial specialization with T1 = int */
template <typename T2, typename T3>                                     // one parameter specialized
struct Foo<int, T2, T3> {
    void print() { std::cout << "partial #1a\n"; }
};

/* #1b partial specialization with first = float */
template <typename T, typename U>                                       // one parameter specialized
struct Foo<float, T, U> {                                               // typenames can be anything
    void print() { std::cout << "partial #1b\n"; }
};                                              

/* #2 partial specialization with T1 = int and T3 = char */
template <typename T2>                                                  // two parameters specialized
struct Foo<int, T2, char> {
    void print() { std::cout << "partial #2\n"; }
};

/* #3 full specialization with T1 = T2 = double and T3 = int */
template <>                                                             // empty as full specialization
struct Foo<double, double, int> {
    void print() { std::cout << "full\n"; }
};

int main() {
    Foo<int, double, float> fooA;
    fooA.print();                                                       // #1a called
    
    Foo<float, double, float> fooB;
    fooB.print();                                                       // #1b called

    Foo<char, double, float> fooC;                                      // primary called

    return 0;
}
