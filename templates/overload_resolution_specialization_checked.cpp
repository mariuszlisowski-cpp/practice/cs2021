#include <iostream>

/* #1: overload for all types */
template<class T>
void f(T);                                                              // checked as 1st (as overload)

/* #2:  overload for all pointer types */
template<class T>
void f(T*);                                                             // checked as 2nd (as overload)

/* #2a: specialization of #2 */
template<>
void f(int*);                                                           // specialization checked also


int main() {
    f(new int{1});                                                      // 1st overload rejected
                                                                        // 2nd accepted (it's specialization)

    return 0;
}
