#include <iostream>

/* primary template */
template <typename T1, typename T2, typename I>
class A {};

/* #1: partial specialization where T2 is a pointer to T1 */
template<typename T, typename I>
class A<T, T*, I> {};

/* #2: partial specialization where T1 is a pointer */
template<typename T, typename T2, typename I>
class A<T*, T2, I> {};

/* #3: partial specialization where T1 is int, I is double,
       and T2 is a pointer */
template<typename T>
class A<int, T*, double> {};

/* #4: partial specialization where T2 is a pointer */
template<typename X, typename T, typename I>
class A<X, T*, I> {};

int main() {

    return 0;
}
