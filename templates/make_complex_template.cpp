#include <complex>
#include <iostream>

template <typename T, typename U>
std::complex<T> makeComplex(T first, U second) {
    return first + second;
}

int main() {
    std::complex<int> a = makeComplex(4, 5);            // both ints
    std::complex<double> b = makeComplex(3.2, 2.1);     // both doubles
    std::complex<int> c = makeComplex(1, 5.7);          // int, double -> takes int

    std::cout << a << std::endl;
    std::cout << b << std::endl;
    std::cout << c << std::endl;

    return 0;
}
