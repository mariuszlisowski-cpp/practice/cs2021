#include <iostream>
#include <string>

template<typename T, typename U>
class Foo {
public:
    Foo(const T& lhs, const T& rhs) : lhs_(lhs), rhs_(rhs) {}
    Foo(const T&, const U&) = delete;
    Foo(T&& lhs, U&& rhs) : lhs_(std::move(lhs)), rhs_(std::move(rhs)) {}   // move c'tor

private:
    T lhs_;
    U rhs_;
};

// decutction guides
template<typename T, typename U>
Foo(const T& lhs, const U& rhs) -> Foo<T, U>;

template<typename T>
Foo(T, const char*) -> Foo<T, std::string>;                                 // cstring converted to string

template<typename T>
Foo(const char*, T) -> Foo<std::string, T>;


int main() {
    Foo foo(42, "cstring");                                                 // move c'tor called


    return 0;
}
