#include <complex>
#include <iostream>


template <typename Type>
Type add(Type first, Type second) {
    return first + second;
}

int main() {
    auto result_complexA = add<std::complex<int>>({3, 4}, {5, 6});
    auto result_complexB = add(std::complex<int>{3, 4}, std::complex<int>{5, 6});

    std::cout << result_complexA << std::endl;    
    std::cout << result_complexB << std::endl;

    return 0;
}
