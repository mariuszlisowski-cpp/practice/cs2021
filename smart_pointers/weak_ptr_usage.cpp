#include <iostream>
#include <memory>

struct Msg {
    int value{ 42 };
};

template<typename T>
void checkMe(std::weak_ptr<T>& wp) {
    std::shared_ptr<T> spB = wp.lock();                                     // returns shared_ptr or nullptr
    std::cout << "count: " << spB.use_count() << std::endl;                 // counter incremented (if not nullptr)
    /* always check wether conversion to shared_ptr is not nullptr */
    if (spB) {
        std::cout << spB->value << std::endl;
    } else {
        std::cout << "expired" << std::endl;
    }
}

int main() {
    auto spA = std::shared_ptr<Msg>(new Msg{});
    std::cout << "count: " << spA.use_count() << std::endl;    

    auto wp = std::weak_ptr<Msg>(spA);
    checkMe(wp);

    /* shared is nullptr now */
    spA.reset();                                                            // count 0
    checkMe(wp);

    return 0;
}
