/* auto p = new Data(10) means:
   - allocate sizeof(Data) bajtów on heap
   - call c'tor of Data
   - assign address to poiner p

   The order of evaluation of operands of almost all C++ operators(including the order
   of evaluationof function arguments in a function-call expression and the order of evaluation
   of the subexpressions within any expression) is unspecified.
*/
#include <memory>

struct Data {
    int value;
};

void sink(std::shared_ptr<Data> oldData,
          std::shared_ptr<Data> newData)
{
    // ...
}

/* memory leak possible */
int main() {
    sink(std::shared_ptr<Data>{new Data{41}},                   // unspecified order of evaluation of operands
         std::shared_ptr<Data>{new Data{42}});                  // till c++17
                                                                    // one c'tor can throw an exception
    return 0;                                                       // while the other object's memory has not been
}                                                                   // yet assigned to a pointer causing LEAK (!)
