#include <memory>

struct Node {
    std::shared_ptr<Node> curr;
    std::shared_ptr<Node> next;
};

int main () {
    auto a = std::shared_ptr<Node>(new Node);
    auto b = std::shared_ptr<Node>(new Node);

    a->curr = b;
    b->next = a;
}                                                       // memory leak

/*  _______________
    | curr | next | Node a
       |              ^
       |              |
       |              |
       v   _______________
    Node b | curr | next |
*/
