#include <iostream>
#include <memory>

auto make_unique() {
    return std::make_unique<int>(42);
}

void use_unique_by_value(std::unique_ptr<int> uptr) {
    std::cout << *uptr << std::endl;
}

void use_unique_by_ref(std::unique_ptr<int>& uptr) {
    std::cout << *uptr << std::endl;
}

int main() {
    use_unique_by_value(make_unique());                             // ok as temporary object passed (rvalue)

    auto uptr = make_unique();                                      // local object created

    use_unique_by_ref(uptr);                                        // accepted by reference thus no move needed
    use_unique_by_value(std::move(uptr));                           // must be moved (as accepted by value)

    if (!uptr) {
        std::cout << "uptr is nullptr" << std::endl;
    } else {
        uptr.get();                                                  // UB: uptr does NOT exist in this scope
    }

    uptr.reset(new int(42));                                         // smart pointer reused
    auto ptr = std::move(uptr);                                      // but ownership passed again

    return 0;
}
