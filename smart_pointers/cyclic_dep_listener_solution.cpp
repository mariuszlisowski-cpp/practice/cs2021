/* By having a weak reference to a Listener, we have separated the lifetimes of Source and Listener.
   A Source converts the weak_ptr<Listener> to a temporary shared_ptr<Listener> on-demand
   when it has to dispatch an event. When a Listener is destroyed, its Source cannot forward the events,
   and that can be handled appropriately depending on the application.
 */
#include <iostream>
#include <memory>

struct Event {};

class Listener {
public:
    void onEvent(Event e) {
        std::cout << "event handled\n";
    }
};

class Source {
public:
    void dispatchEvent(Event e) {
        // acquire strong ref to listener
        if (auto listener = weakListener.lock()) {                      // temporary shared_ptr to Listener
            listener->onEvent(e);
        } else {
            std::cout << "no listeners\n";
        }
    }

    void registerListener(const std::shared_ptr<Listener>& lp) {
        weakListener = lp;
    }

private:
    // weak reference to Listener
    std::weak_ptr<Listener> weakListener;
};

int main() {
    auto source = std::make_shared<Source>();
    auto listener = std::make_shared<Listener>();

    source->registerListener(listener);
    source->dispatchEvent(Event());
    
    listener.reset();                                                       // no listener
    source->dispatchEvent(Event());                                         // nothing to do

    return 0;
}
