#include <iostream>
#include <memory>

struct Node {
    std::shared_ptr<Node> curr;
    std::weak_ptr<Node> next;
};

int main () {
    auto a = std::shared_ptr<Node>(new Node);
    auto b = std::shared_ptr<Node>(new Node);
    std::cout << a.use_count() << std::endl;
    std::cout << b.use_count() << std::endl;
    a->curr = b;
    b->next = a;
}                                                       // NO memory leak (solved by weak_ptr)

/*  _______________
    | curr | next | Node a
       |              ^
       |              | 
       |              | weak_ptr
       v   _______________
    Node b | curr | next |
*/
