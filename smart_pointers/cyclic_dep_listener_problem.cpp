/* But the above design causes the lifetime of a Listener to be influenced by the lifetime of a Source. The
   situation could be further exacerbated if a Listener is a big object and stays in memory for longer than it should.
   PROBMEM: The existence of a Source and a Listener should be mutually independent,
            and only their respective holders should control their lifetimes.
*/
#include <iostream>
#include <memory>

struct Event {};

class Listener {
public:
    void onEvent(Event e) {
        std::cout << "event handled\n";
    }
};

class Source {
public:
    void dispatchEvent(Event e) {
        if (listener) {
            listener->onEvent(e);
        } else {
            std::cout << "no listeners\n";
        }
    }
    void registerListener(const std::shared_ptr<Listener>& lp) {
        listener = lp;
    }

private:
    // strong reference to Listener
    std::shared_ptr<Listener> listener;
};

int main() {
    auto source = std::make_shared<Source>();
    auto listener = std::make_shared<Listener>();

    source->registerListener(listener);
    source->dispatchEvent(Event());
    
    listener.reset();                                                       // no listener
    source->dispatchEvent(Event());                                         // but event STILL handled (!)
    
    return 0;
}
