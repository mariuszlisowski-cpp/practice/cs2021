#include <numeric>
#include <iostream>
#include <vector>

template <typename T>
void print_container(const T container) {
    for (const auto& el : container) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::vector<int> v(10);                                 // ten elements

    std::iota(begin(v), end(v), 0);                         // initial value is zero

    print_container(v);

    return 0;

}
