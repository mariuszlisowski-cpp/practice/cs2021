#include <algorithm>
#include <iostream>
#include <vector>

template <typename T>
void print_container(const T container) {
    for (const auto& el : container) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::vector<int> v(10);                                 // ten elements

    std::generate(begin(v), end(v),
                  [i{0}]() mutable {                        // initialize with zero to decude
                      return i++;                           // and make it mutable
                  });

    print_container(v);

    return 0;
}
