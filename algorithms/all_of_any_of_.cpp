#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> v{ 8, 2, 5, 3, 4, 4, 2, 7, 6, 6, 1, 8, 9, 0 };

    auto isAnyLessThanOne =
    std::any_of(begin(v), end(v),
                [](auto el) {
                    return el < 1;
                });
    std::cout << std::boolalpha << isAnyLessThanOne << std::endl;

    auto areAllBiggnrThanOne =
    std::all_of(begin(v), end(v),
                [](auto el) {
                    return el > 1;
                });
    std::cout << std::boolalpha << areAllBiggnrThanOne << std::endl;

    return 0;
}
