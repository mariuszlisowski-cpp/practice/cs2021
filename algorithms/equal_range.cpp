#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template<typename T>
void print(const T& container) {
    std::copy(container.begin(), container.end(),
              std::ostream_iterator<typename T::value_type>(std::cout, " "));
    std::cout << '\n';
}

int main() {
    std::vector<int> v{ 2, 5, 5, 1, 4, 2, 5, 6 };
    std::sort(begin(v), end(v));                                                // must be sorted
    print(v);

    auto [lowerA, upperB] = std::equal_range(begin(v), end(v), 5);              // existing element
    /* { 1, 2, 2, 4, 5, 5, 5, 6}
                     l        u                                                 // lower (inclusive) upper (exclusive) 
                                                                                // lower: not less than ...
                                                                                // upper: greater than ...
    */

    auto [lowerB, upperB] = std::equal_range(begin(v), end(v), 3);              // non-existing element
    /* { 1, 2, 2, 4, 5, 5, 5, 6}
                 lu                                                             // lower (inclusive) upper (exclusive) 
                                                                                // lower: not less than ...
                                                                                // upper: greater than ...
    */

    return 0;
}
