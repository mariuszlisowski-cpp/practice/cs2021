#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

struct Neighbour {
   int floor_;
   std::string name_;
   Neighbour(int f, std::string name) : floor_(f), name_(name) {}

   friend std::ostream& operator<<(std::ostream& os, const Neighbour& neighbour);
};

std::ostream& operator<<(std::ostream& os, const Neighbour& neighbour) {
    os << neighbour.floor_ << ' ' << neighbour.name_ << '\n';

    return os;
}

template <typename T>
void print(std::vector<T>& vec) {
    for (const auto& el : vec) {
        std::cout << el;
    }
    std::cout << '\n';
}

template <typename T>
void sort(std::vector<T> vec) {
    std::sort(vec.begin(), vec.end(),
              [](Neighbour a, Neighbour b){
                  return a.name_ < b.name_;
              });
    print(vec);
}

template <typename T>
void stable_sort(std::vector<T> vec) {
    std::stable_sort(vec.begin(), vec.end(),
                     [](Neighbour a, Neighbour b){
                         return a.name_ < b.name_;
                     });
    print(vec);
}

int main() {
    std::vector<Neighbour> vec = {
        Neighbour(3, "Peter"),
        Neighbour(1, "Bob"), 
        Neighbour(4, "Bob"),
        Neighbour(2, "Anna"),
    };

    sort(vec);
    /*  The result might be:
        2 Anna      2 Anna
        1 Bob       4 Bob           // here
        4 Bob       1 Bob           // or here
        3 Peter     3 Peter */
    
    stable_sort(vec);
    /*  The result is always:
        2 Anna
        1 Bob                       // always first
        4 Bob                       // always second    
        3 Peter
    With stable_sort, it is ensured that you always will get the first result.
    With the duplicates in the same order they were in the initial list. */

    return 0;
}
