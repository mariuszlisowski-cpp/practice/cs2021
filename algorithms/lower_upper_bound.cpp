#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template<typename T>
void print(const T& container) {
    std::copy(container.begin(), container.end(),
              std::ostream_iterator<typename T::value_type>(std::cout, " "));
    std::cout << '\n';
}

int main() {
    std::vector<int> v{ 2, 5, 5, 1, 4, 2, 5, 6 };
    std::sort(begin(v), end(v));                                                // must be sorted
    print(v);

    auto lower = std::lower_bound(begin(v), end(v), 5);                         // lower (inclusive)
                                                                                // lower: not less than ...
    std::cout << *lower << std::endl;


    auto upper = std::upper_bound(begin(v), end(v), 5);                         // upper (exclusive) 
                                                                                // upper: greater than ...
    std::cout << *upper << std::endl;

    return 0;
}
