#include <algorithm>
#include <iostream>
#include <list>
#include <vector>

template <typename T>
void print_container(const T container) {
    for (const auto& el : container) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::vector<int> v{ 1, 2, 3, 4, 5, 6, 7, 8 };
    std::list<int> l{ 10, 20, 30, 40, 50, 60, 70, 80 };
    std::vector<int> res;
    res.resize(v.size());

    std::transform(begin(v), end(v),                                // first input container
                   begin(l),                                        // second input container
                   begin(res),                                      // writes here
                   [](auto v_el, auto l_el) {
                       return v_el + l_el;                          // concatenate containers
                   });

    print_container(res);

    return 0;
}
