#include <execution>
#include <iostream>
#include <numeric>
#include <vector>

template<template <typename, typename> class Container,
         typename T,
         typename Allocator = std::allocator<T> >
T get_product(const Container<T, Allocator>& container){
    return std::accumulate(begin(container), end(container),
                           1,                                       // neutral for multiplication
                           std::multiplies<T>());
}

int main() {
    std::vector<int> v{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    std::cout << std::accumulate(begin(v), end(v), 0) << '\n';      // initial zero to add up
    std::cout << get_product(v) << '\n';

    /* same as accumulate but preferable (c++17, c++20) */
    std::reduce(std::executon::par, begin(v), end(v),               // parallel execution (faster than sequential)
                1,
                std::multiplies<int>());

    return 0;
}
