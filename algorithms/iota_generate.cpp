#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

template<typename CONTAINER>
void print(CONTAINER container) {
    for (const auto& el : container) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::vector<int> v(19);
    
    std::iota(begin(v), end(v), -9);
    print(v);

    /* iota equivalent */
    std::generate(begin(v), end(v),
                  [i{ -9 }]() mutable { return i++; });
    print(v);

    return 0;
}
