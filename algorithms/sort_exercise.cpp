#include <algorithm>
#include <deque>
#include <iostream>
#include <iterator>

struct Point {
    int x;
    int y;

    friend std::ostream& operator<<(std::ostream& os, const Point& point);
};

std::ostream& operator<<(std::ostream& os, const Point& point) {
    os << "(" << point.x << ", " << point.y << ")";

    return os;
}

void print(const std::deque<Point>& deq) {
    std::copy(deq.begin(), deq.end(), std::ostream_iterator<Point>(std::cout, " "));
    std::cout << '\n';
}

int main() {
    std::deque<Point> d{
        { 1, 3 },
        { 0, 0 },
        { 1, 2 },
        { 2, 4 },
        { 4, 1 },
        { 0, 2 },
        { 2, 2 }
    };

    auto pointXCompare{ [](const auto& lhs, const auto& rhs) {
        return lhs.x < rhs.x;
    }};
    auto pointYCompare{ [](const auto& lhs, const auto& rhs) {
        return lhs.y < rhs.y;
    }};

    std::cout << std::boolalpha << std::is_sorted(begin(d), end(d), pointXCompare) << '\n';
    std::cout << std::boolalpha << std::is_sorted(begin(d), end(d), pointYCompare) << '\n';
    print(d);

    std::stable_sort(d.begin(), d.end(), pointXCompare);        // stable
    print(d);
    std::cout << std::boolalpha << std::is_sorted(begin(d), end(d), pointXCompare) << '\n';
    
    std::sort(d.begin(), d.end(), pointYCompare);               // not stable
    print(d);
    std::cout << std::boolalpha << std::is_sorted(begin(d), end(d), pointYCompare) << '\n';

    return 0;
}
