#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

int main() {
    std::vector<int>v(10, 2);                                           // {2, 2, 2, 2, 2, 2, 2, 2, 2, 2}

    std::partial_sum(begin(v),
                     end(v),
                     std::ostream_iterator<int>(std::cout, " "));       // writes to console
    
    /* {2, 2, 2, 2, 2, 2, 2, 2, 2, 2}
        2   
           4                                                            // 2 + 2
              6                                                         // 2 + 2 + 2
                 ...               20
    */

    /* same as std::inclusive_scan */
    std::partial_sum(begin(v),
                     end(v),
                     std::ostream_iterator<int>(std::cout, " "),
                     std::multiplies<int>());                           // binary predicate

    /* {2, 2, 2, 2, 2, 2, 2, 2, 2, 2}
        2   
           4                                                            // 2 * 2
              8                                                         // 2 * 2 * 2
                 ...               1024
    */

    return 0;
}
