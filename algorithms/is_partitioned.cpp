#include <algorithm>
#include <array>
#include <iostream>
#include <iterator>
#include <vector>

int main() {
    bool is_partitioned;
    std::cout << std::boolalpha;

    auto is_even = [](auto i) { return i % 2; };                                // check for even number

    std::array<int, 9> a1 = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::cout << std::is_partitioned(begin(a1), end(a1), is_even) << '\n';      // false (mix of odd & even)

    std::array<int, 5> a2 = { 2, 4, 6, 8, 9 };
    std::cout << std::is_partitioned(begin(a2), end(a2), is_even) << '\n';      // false (saa)

    std::vector v1 = {9, 5, 3};
    std::cout << std::is_partitioned(v1.begin(), v1.end(), is_even) << '\n';    // true (all odd)

    std::vector v2 = {2, 4, 6, 8};
    std::cout << std::is_partitioned(v2.begin(), v2.end(), is_even) << '\n';    // true (all even)

    std::array<int, 100> a4 = {9, 5, 3};                                        // rest of zeroes
    std::cout << std::is_partitioned(a4.begin(), a4.end(), is_even) << '\n';    // true (all odd) zeros ignored?

    std::array<int, 100> a3 = {10, 12, 2};                                      // rest of zeroes
    std::cout << std::is_partitioned(a3.begin(), a3.end(), is_even) << '\n';    // true (all even)

    return 0;
}
