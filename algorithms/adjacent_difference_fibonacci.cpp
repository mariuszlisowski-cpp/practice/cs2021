#include <array>
#include <functional>
#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

template <typename T>
void print(const T& container) {
    std::copy(begin(container), end(container), std::ostream_iterator<int>{ std::cout, " " });
    std::cout << '\n';
}

int main() {
    // default implementation - the difference between two adjacent items
    std::vector v{ 2, 4, 6, 8, 10, 20 };
    std::adjacent_difference(v.begin(), v.end(), v.begin());
    print(v);

    // Fibonacci
    std::array<int, 10> a{ 1 };
    print(a);
    adjacent_difference(begin(a), std::prev(end(a)), std::next(begin(a)), std::plus<>{});
    print(a);

    return 0;
}
