#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

template <typename T>
void print_container(const T container) {
    for (const auto& el : container) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::vector<std::pair<int, std::string>> v1{
        { 0, "Zero" }, { 1, "One" }, { 2, "Two" }, { 3, "Three" }, { 4, "Four" }, { 5, "Five" }
    };

    /* new container containing only first element of the above pairs */
    // slower
    std::vector<int> v2;
    std::transform(begin(v1), end(v1),
                   std::back_inserter(v2),                                  // may allocate new memory
                   [](const auto& pair) {
                       return pair.first;
                   });
    print_container(v2);
    // faster
    std::vector<int> v2a(v1.size());                                        // allocate memory once
    std::transform(begin(v1), end(v1),
                   begin(v2a),
                   [](const auto& pair) {
                       return pair.first;
                   });
    print_container(v2a);

    /* new container containing the second element and a colon */
    std::vector<std::string> v3;
    std::transform(begin(v1), end(v1),
                   std::back_inserter(v3),
                   [](const auto& pair) {
                       return pair.second + ":" + std::to_string(pair.first);
                   });
    print_container(v3);

    /* new container containing every other alphabet letter */
    std::vector<char> v4;
    constexpr size_t ALPHABET_SIZE{ 26 };
    std::generate_n(std::back_inserter(v4),
                    ALPHABET_SIZE / 2,
                    [ch{'_'}]() mutable {
                        return ch += 2;
                    });
    print_container(v4);

    return 0;
}
