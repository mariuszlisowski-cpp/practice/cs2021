#include <algorithm>
#include <iostream>
#include <functional>
#include <vector>

template <typename T>
void printAllGreaterThanSix(const std::vector<T>& v) {
    auto first = v.begin();
    while (first != v.end()) {
        first = std::find_if(first, end(v),
                             [](auto e) {
                                 return e > 6;
                             });
        if (first != v.end()) {
            std::cout << *first << ' ';
            ++first;
        }
    }
    std::cout << '\n';
}

void searchForRange(const std::vector<int>& v, const std::vector<int>& range) {
    auto it = std::search(v.begin(), v.end(),
                          std::default_searcher(begin(range), end(range)));
    if (it != v.end()) {
        std::cout << "\nfound at index: " << it - v.begin();                        // std::distance(v.begin(), it)
    } else {
        std::cout << "\nnot found!" << std::endl;
    }

}
void searchForRepeatedValue(const std::vector<int>& v, int repeatedValue) {
    auto it = std::search_n(v.begin(), v.end(), 2, repeatedValue);
    if (it != v.end()) {
        std::cout << "\nfound at index: " << std::distance(v.begin(), it);          // it - v.begin()
    }

}

int main() {
    std::vector<int> v{ 8, 2, 5, 3, 4, 4, 2, 7, 6, 6, 1, 8, 9 };

    printAllGreaterThanSix(v);

    /* find all equal to 2, 4, 6 or 8 */
    auto first = v.begin();
    while (first != v.end()) {
        first = std::find_if(first, end(v),
                             [](auto e) {
                                 return e == 2 or e == 4 or e == 6 or e == 8;
                             });
        if (first != v.end()) {
            std::cout << *first << ' ';
            ++first;
        }
    }

    /* find two consecutive equal elements */
    std::vector rangeA{ 6, 6 };
    searchForRange(v, rangeA);
    searchForRepeatedValue(v, 6);

    std::vector rangeB{ 7, 7 };
    searchForRange(v, rangeB);
    searchForRepeatedValue(v, 7);

    return 0;
}
