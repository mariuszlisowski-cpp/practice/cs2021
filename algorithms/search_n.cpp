#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector v{ 1, 2, 3, -1, -1, -1, 4, 5 };

    auto it = std::search_n(begin(v), end(v), 3, -1);           // find three numbers
    if (it != v.end()) {
        auto dist = std::distance(v.begin(), it);
        std::cout << "found at index: " << dist << std::endl;   // beginning of first occurence
    }

    return 0;
}
