#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template<typename T>
void print(const T& constainer) {
    std::copy(begin(constainer), end(constainer),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    auto is_even = [](auto i) { return i % 2; };                                // check for even number

    std::vector v = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::vector<int> trues;
    std::vector<int> falses;
    
    std::partition_copy(v.begin(), v.end(),
                        std::back_inserter(trues),
                        std::back_inserter(falses),
                        is_even);
    print(trues);
    print(falses);

    return 0;
}
