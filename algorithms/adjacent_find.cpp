#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector v{ 1, 2, 3, 4, 4, 5 };

    /* searches the range for two consecutive equal elements */
    auto it = std::adjacent_find(begin(v), end(v));             // any adjacent elements?
    if (it != v.end()) {
        std::cout << *it << std::endl;                          // beginning of first adjacent element
    }

    return 0;
}
