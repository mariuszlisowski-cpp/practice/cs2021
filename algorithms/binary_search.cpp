#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

template<typename T>
void print(const T& container) {
    std::copy(container.begin(), container.end(),
              std::ostream_iterator<typename T::value_type>(std::cout, " "));
    std::cout << '\n';
}

int main() {
    std::vector<int> v{ 2, 5, 5, 1, 4, 2, 5, 6 };
    std::sort(begin(v), end(v));                                                // must be sorted
    print(v);

    auto is_contained = std::binary_search(begin(v), end(v), 4);                // returns bool
                                                                                // use lower/upper bound
                                                                                // to get iterator(s)
    std::cout << std::boolalpha << is_contained << std::endl;

    return 0;
}
