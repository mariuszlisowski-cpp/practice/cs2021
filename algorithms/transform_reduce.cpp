#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

int main() {
    std::vector<int> a{0, 1, 2, 3, 4};
    std::vector<int> b{5, 4, 2, 3, 1};

    /* same as std::inner_product (iloczyn skalarny) */
    auto r1 = std::transform_reduce(a.begin(), a.end(),
                                    b.begin(),                          // lenght at least of the first container
                                    0);                                 // init (neutral for addition)
        0*5 + 1*4 + 2*2 + 3*3 + 4*1
        0 + 4 + 4 + 9 + 4 = 21   
    */
    std::cout << r1 << std::endl;

    /* can be run concurrently (std::execution::par) */
    auto r2 = std::transform_reduce(std::execution::par,
                                    a.begin(), a.end(),
                                    b.begin(),                          // lenght at least of the first container
                                    0,                                  // init (neutral for addition)
                                    std::plus<>(),
                                    std::equal_to<>());                 // bool operation
    /* (0 == 5) + (1 == 4) + (2 == 2) + (3 == 3) + (4 == 1)
         false      false      true       true       false
           0    +     0    +     1    +    1     +     0    =   2       
    */
    std::cout << r2 << std::endl;                                       // same positions count of both vector
    /*  {0, 1, 2, 3, 4};
        {5, 4, 2, 3, 1};
               x  x                                                     // same positions
    */

    return 0;
}
