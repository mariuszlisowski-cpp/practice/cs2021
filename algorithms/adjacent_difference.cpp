#include <iostream>
#include <iterator>
#include <numeric>
#include <vector>

int main() {
    std::vector v{ 2, 3, 6, 9, 12, 22 };                                    // six elements
                                                                            // but notice:
    std::adjacent_difference(begin(v),                                      // five differences
                             end(v),                                        // plus initial
                             std::ostream_iterator<int>(std::cout, " "));

    /* {2,  3,  6,  9, 11,  20}
        2                                                                   // 2 - 0 (zero as neutral initializer)
          1   3   3   3   10                                                // 3 - 2, 6 - 3, 9 - 6, 11 - 9, 20 - 11
    */

    std::vector w{ -100, 2, 4, 8, 16, 32 };
    std::adjacent_difference(w.begin(),
                             w.end(),
                             std::ostream_iterator<int>(std::cout, " "));
    // {-100, 102, 2, 4, 8, 16}                                             // first element stays the same

    return 0;
}
