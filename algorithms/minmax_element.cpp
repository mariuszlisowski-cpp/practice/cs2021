#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    /* iterators returned */
    std::vector<int> v{ 9, 1, 2, 3, 4, 5 }; 
    auto [min_el, max_el] = std::minmax_element(v.begin(), v.end());    // iterators
    std::cout << *min_el << ' ' << *max_el << std::endl;

    min_el = std::min_element(v.begin(), v.end());                      // saa
    std::cout << *min_el << std::endl;

    max_el = std::max_element(v.begin(), v.end());
    std::cout << *max_el << std::endl;

    /* values returned */
    auto [min, max] = std::minmax({ 9, 1, 2, 3, 4, 5 });                // initializer list
    std::cout << min << ' ' << max << std::endl;

    min = std::min(-1, 1);                                              // or values
    std::cout << min << std::endl;

    max = std::max(-1, 1);                                              // saa
    std::cout << max << std::endl;

    return 0;
}
