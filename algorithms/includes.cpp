#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector<int> v1 {1, 2, 5, 5, 5, 9};
    std::vector<int> v2 {2, 5, 7};

    std::cout << std::boolalpha
              << std::includes(begin(v1), end(v1),
                               begin(v2), end(v2)) << '\n';         // false (no 7 in first container)

    std::vector<int> v3 {2, 5, 9};
    std::cout << std::boolalpha
              << std::includes(begin(v1), end(v1),
                               begin(v3), end(v3)) << '\n';         // true (all fron second in first)

    return 0;
}
