#include <algorithm>
#include <iostream>
#include <vector>

template <typename T>
void print_container(const T container) {
    for (const auto& el : container) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

int main() {
    std::vector<int> v;                                     // empty container

    std::generate_n(std::back_inserter(v),                  // pushing back
                    10,
                    [i{0}]() mutable {                      // must be zero to decuce type
                        return i++;
                    });

    print_container(v);

    return 0;
}
