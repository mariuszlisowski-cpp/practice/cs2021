#include <algorithm>
#include <iostream>
#include <vector>

int main() {
    std::vector v{ 3, 1, 4, 2, 5 };
    auto result = std::none_of(v.begin(), v.end(),
                               [](auto el) {
                                   return el < 1;
                               });

    std::cout << std::boolalpha << result << std::endl;

    return 0;
}
