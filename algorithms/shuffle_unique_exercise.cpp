#include <algorithm>
#include <iostream>
#include <iterator>
#include <random>
#include <vector>

template<typename T>
void print(const T& container) {
    std::copy(begin(container), end(container),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

int main() {
    std::vector<int> v1{ 7, 6, 6, 1, 8, 9, 9 };
    std::vector<int> v2{ 7, 6, 6, 1, 8, 9, 9 };

    /* remove all consecutive duplicated elements */
    std::vector<int>::iterator it;
    while ((it = std::adjacent_find(v1.begin(), v1.end())) != v1.end()) {
        v1.erase(it, it + 2);
    }
    print(v1);

    /* remove consecutive duplicated elements */
    auto last = std::unique(v2.begin(), v2.end());                                 // moves unwanted to the end
    v2.erase(last, v2.end());                                                      // erases unwanted from the end
    print(v2);

    /* shuffle randomly */
    std::shuffle(v2.begin(), v2.end(), std::mt19937(std::random_device{}()));      // should output different every time
    print(v2);

    return 0;
}
